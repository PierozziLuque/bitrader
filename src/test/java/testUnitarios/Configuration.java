package testUnitarios;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import bitrader.config.ConfigContent;

import static org.junit.Assert.assertTrue;
public class Configuration {
	private JsonObject jsonObject;
	ConfigContent configContent;
	JsonArray rules;
	
	@Before
	public void init() {
		jsonObject = new JsonObject();
		rules = new JsonArray(1);
		String rule = "{\"name\":\"BITCOIN\", \"symbol\":\"BTC\",\"quantity\":1,\"type\":\"BUY\","+"\"percentage\":1}";
		JsonParser parser = new JsonParser();
		rules.add(parser.parse(rule));
	}
	
	@Test
	public void testParseOk(){
		jsonObject.add("frecuency", new JsonPrimitive(1));
		jsonObject.add("amount", new JsonPrimitive(100));
		jsonObject.add("rules", rules);
		configContent = new ConfigContent(jsonObject);
		assertTrue(configContent.create());
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testParseFailKeyFrecuencyNotExists(){
		jsonObject.add("amount",new JsonPrimitive(1));
		jsonObject.add("rules", rules);
		configContent = new ConfigContent(jsonObject);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testParseFailKeyRulesNotExists(){
		jsonObject.add("frecuency", new JsonPrimitive(1));
		jsonObject.add("amount",new JsonPrimitive(1));
		configContent = new ConfigContent(jsonObject);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testParseFailInvalidRules(){
		jsonObject.add("frecuency", new JsonPrimitive(1));
		jsonObject.add("amount",new JsonPrimitive(1));
		jsonObject.add("rules", new JsonArray());
		configContent = new ConfigContent(jsonObject);
		configContent.create();
	}

}
