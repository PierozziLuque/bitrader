package testUnitarios;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import bot.BotSetupDTO;
import setup.SetupCreator;

public class BotCreatorTest {
	private JsonObject discoverJson;
	private JsonObject configJson;
	JsonArray rules;
	
	@Before
	public void init() {
		discoverJson = new JsonObject();
		configJson = new JsonObject();
		rules = new JsonArray(1);
		String rule = "{\"name\":\"BITCOIN\", \"symbol\":\"BTC\",\"quantity\":1,\"type\":\"BUY\","+"\"percentage\":1}";
		JsonParser parser = new JsonParser();
		rules.add(parser.parse(rule));	
	}
	

	@Test
	public void testCreateOk() {
		createConfigJsonOk();
		createDiscoverJsonOk();
		BotSetupDTO setupCreated = SetupCreator.create(discoverJson,configJson);
		assertTrue(setupCreated.getLoopTime() == 1);
		assertTrue(setupCreated.getRules().size()==1);
		assertTrue(setupCreated.getPath()!=null);
		assertTrue(setupCreated.getPackage().equals("package"));
		assertTrue(setupCreated.getAmount()==100);
	}
	
	@Test
	public void testCreateFailPackageNull() {
		createConfigJsonOk();
		BotSetupDTO setupCreated = SetupCreator.create(discoverJson,configJson);
		assertTrue(setupCreated == null );
	}

	@Test
	public void testCreateFailConfigJsonNull() {
		createDiscoverJsonOk();
		BotSetupDTO setupCreated = SetupCreator.create(discoverJson,configJson);
		assertTrue(setupCreated == null );
	}
	
	@Test
	public void testCreateBotGetJsonObjectFail() {
		assertTrue(SetupCreator.getJsonObject("something")==null);
	}
	
	private void createConfigJsonOk()
	{
		configJson.add("frecuency", new JsonPrimitive(1));
		configJson.add("amount", new JsonPrimitive(100));
		configJson.add("rules", rules);
	}
	
	private void createDiscoverJsonOk()
	{
		discoverJson.add("package", new JsonPrimitive("package"));
	}
}
