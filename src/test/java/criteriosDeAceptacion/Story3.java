package criteriosDeAceptacion;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import asset.Asset;
import operation.OperationType;
import plugins.BinanceImplementation;

public class Story3 {
	private BinanceImplementation binance;
	double quantity;
	double marketPrice;
	Asset asset;
	OperationType operationType;
	
    @Mock
    BinanceImplementation binanceFake;
    
	@Before
	public void init() {
		binance = new BinanceImplementation();
		quantity = 5;
		marketPrice = 100;
		asset = new Asset("BITCOIN","BTC");
		MockitoAnnotations.openMocks(this);
	}
	
	@Test
    public void testGetPriceTimeoutExceeded()  
    { 
		binance.setTimeout(1);
		assertTrue(binance.getPrice("BTC")==-1);
    }
	
	@Test
    public void testGetPriceOk()  
    { 
		Mockito.when(binanceFake.getPrice("BTC")).thenReturn(100.0);
		assertTrue(binanceFake.getPrice("BTC")==100);
    }
	
	@Test
    public void testGetPriceFail()  
    { 
		Mockito.when(binanceFake.getPrice("BTC")).thenReturn(-1.0);
		assertTrue(binanceFake.getPrice("BTC")==-1.0);
    }
}
