package bitrader;

import com.google.gson.JsonObject;

import bot.BotSetupDTO;
import setup.SetupCreator;
import ui.BotConfigView;
import ui.BotCreator;
import ui.SetupMarkets;

public class BiTrader {
	public static void main(String[] args) {
		try {
			BotSetupDTO botSetup= BiTrader.createSetup();
			
			SetupMarkets setupMarkets = new SetupMarkets(botSetup.getPath(), botSetup.getPackage());
			setupMarkets.initialize();
			
			BotConfigView view = new BotConfigView(setupMarkets.getMarketNames());
			view.show();
			
			BotCreator creator = new BotCreator(botSetup, setupMarkets);
			creator.attach(view);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static BotSetupDTO createSetup()
	{
		JsonObject discoverJson = SetupCreator.getJsonObject(SetupCreator.discoverName);
		JsonObject configJson = SetupCreator.getJsonObject(SetupCreator.configName);
		
		return SetupCreator.create(discoverJson, configJson);
		
	}
}
