package setup;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import asset.Asset;
import bitrader.config.ConfigContent;
import bot.BotSetupDTO;
import operation.OperationType;
import rules.AssetRule;

public class SetupCreator {
	private static final String userDir = System.getProperty("user.dir");
	public static final String configName = "config.json";
	public static final String discoverName = "discover.json";

	public static BotSetupDTO create(JsonObject discoverJson, JsonObject configJson) {
		try {
			ConfigContent configContent = new ConfigContent(configJson);
			configContent.create();
			List<AssetRule> rules = new ArrayList<AssetRule>();
			String _package = discoverJson.get("package").getAsString();
			String _path = new File("").getAbsolutePath().toString() + "/bin/main/" + _package;
			double amount = configContent.getAmount();
			double[] quantity = configContent.getQuantities();
			double[] percentages = configContent.getPercentages();
			String[] cryptocurrencies = configContent.getNames();
			String[] types = configContent.getTypes();
			String[] symbols = configContent.getSymbols();

			for (int i = 0; i < cryptocurrencies.length; i++) {
				Asset asset = new Asset(cryptocurrencies[i], symbols[i]);
				OperationType type = OperationType.valueOf(types[i]);
				AssetRule assetRule = new AssetRule(percentages[i], asset, quantity[i], type, 0);
				rules.add(assetRule);
			}
			return new BotSetupDTO(configContent.getFrecuency(), amount, rules, _path, _package);

		} catch (Exception e) {
			return null;
		}
	}

	public static JsonObject getJsonObject(String name) {
		String jsonFile = userDir + "/" + name;
		JsonParser parser = new JsonParser();
		try {
			return parser.parse(new FileReader(jsonFile)).getAsJsonObject();
		} catch (Exception e) {
			return null;
		}
	}
}
